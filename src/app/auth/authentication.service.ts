import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { AngularFireAuth } from "@angular/fire/compat/auth";
import { Credentials, CredentialsService } from './credentials.service';

export interface LoginContext {
  username: string;
  password: string;
  remember?: boolean;
}

/**
 * Provides a base for authentication workflow.
 * The login/logout methods should be replaced with proper implementation.
 */
@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  constructor(
    private auth: AngularFireAuth,
    private credentialsService: CredentialsService) { }

  /**
   * Authenticates the user.
   * @param context The login parameters.
   * @return The user credentials.
   */
  async login(context: LoginContext): Promise<Credentials> {
    // Replace by proper authentication call

    return new Promise(res => {
      this.auth.signInWithEmailAndPassword(context.username, context.password)
        .then(value => {
          // console.log('Sucess', value);
          const data = {
            username: context.username,
            token: value.user?.multiFactor['user'].accessToken,
            error: 'none'
          };
          this.credentialsService.setCredentials(data, context.remember);
          res(data)
        })
        .catch(error => {
          // console.log('Something went wrong: ', error);
          res({ username: '', token: '', error: 'error' })
        })
    })
  }

  /**
   * Logs out the user and clear credentials.
   * @return True if the user was logged out successfully.
   */
  logout(): Observable<boolean> {
    // Customize credentials invalidation here
    this.credentialsService.setCredentials();
    return of(true);
  }
}
