import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';

import { QuoteService } from './quote.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as L from 'leaflet';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  quote: string | undefined;
  isLoading = false;

  apiForm!: FormGroup;

  geLocations: Array<any> | undefined;
  estados: any;
  map: any;

  constructor(private quoteService: QuoteService, private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.isLoading = true;

    // Obtener Quote
    this.quoteService
      .getRandomQuote({ category: 'dev' })
      .pipe(
        finalize(() => {
          this.isLoading = false;
        })
      )
      .subscribe((quote: string) => {
        this.quote = quote;
      });
    this.initMap();
    this.createForm();
  }

  createForm() {
    this.apiForm = this.formBuilder.group({
      page: ['', Validators.compose([Validators.required, Validators.min(1)])],
      pageSize: ['', Validators.compose([Validators.required, Validators.min(1), Validators.max(100)])],
    });
  }

  getData() {
    this.geLocations = undefined;
    // Obtener geolocations
    this.quoteService
      .getGeoLocations(this.apiForm.get('page')?.value, this.apiForm.get('pageSize')?.value)
      .pipe(
        finalize(() => {
          this.isLoading = false;
        })
      )
      .subscribe((data: any) => {
        console.log(data);
        this.geLocations = data;
        this.initMap();
      });
  }

  initMap(): void {
    if (!this.map) {
      this.map = L.map('map', {
        center: [23.5236311, -101.9116572],
        zoom: 5,
      });
    }

    this.removeMarker();
    this.addMarkers();
  }

  // Eliminar marcadores en el mapa
  removeMarker() {
    this.map.eachLayer(function (layer: any) {
      layer.remove();
    });
  }

  // Agregar marcadores
  addMarkers() {
    // Agregar capa mapa
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      minZoom: 3,
    }).addTo(this.map);

    // Se agregan los marcadores
    this.geLocations?.map((location) => {
      const marker = L.marker([location.latitude, location.longitude]);
      this.map.addLayer(marker);
      marker.bindPopup(location.skydescriptionlong);
    });
  }
}
