import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

const routes = {
  quote: (c: RandomQuoteContext) => `/jokes/random?category=${c.category}`,
  geoLocations: 'https://api.datos.gob.mx/v1/condiciones-atmosfericas?',
};

export interface RandomQuoteContext {
  // The quote's category: 'dev', 'explicit'...
  category: string;
}

@Injectable({
  providedIn: 'root',
})
export class QuoteService {
  constructor(private httpClient: HttpClient) {}

  getRandomQuote(context: RandomQuoteContext): Observable<string> {
    return this.httpClient.get(routes.quote(context)).pipe(
      map((body: any) => body.value),
      catchError(() => of('Error, could not load joke :-('))
    );
  }

  getGeoLocations(page: number, pageSize: number): Observable<any> {
    return this.httpClient.get(`${routes.geoLocations}page=${page}&pageSize=${pageSize}`).pipe(
      map((body: any) => body.results),
      catchError(() => of('Error, no se pudo obtener la información'))
    );
    // let data = {
    //   operationName: 'getUsers',
    //   query: 'query getUsers($app: String!) { getUsers(app: $app) { error msj users { curp user email active } } }',
    //   variables: { app },
    // }
    // let response = fetch('https://925-review-desarrollo-31hgw0.k8s.sfp.gob.mx/graphql', {
    //   method: 'POST',
    //   headers: {
    //     'Content-Type': 'application/json',
    //     'Accept': 'application/json',
    //   },
    //   body: JSON.stringify(data),
    // })

    // return response
  }
}
